var inDataOdabir = { "projekt": "p_komercijalna", "procedura": "p_get_teme" };


function ucenik() {
    $.ajax({
        type: 'POST',
        url: url,
        data: inDataOdabir,

        success: function (data) {
    
            var jsonBody = JSON.parse(data);
            var errcode = jsonBody.h_errcode;
            var message = jsonBody.h_message;
            var count = jsonBody.count;
           
            var izlaz = inicijalizacijaUc();

            if (message == null || message == "", errcode == null || errcode == 0) {
               
                   document.getElementById("container").innerHTML = izlaz; 
                   $("#example").DataTable({
                     
                       data: jsonBody.data,
                       "columns":[
                     
                           {"data":"NAZIV"},
                           {"data":"PROFESOR"},
                           {"data":"PREDMET"},
                           {
                            "data": "ODABIR",
                            "orderable": false
                           },
                        
                       ],"columnDefs": [
                        { "width": "60px", "targets": 3, "className": "dt-center" }
                       
                      ],
                      rowReorder: {
                        selector: 'tr'
                        
                    },
                      responsive: true,
                       
                       "language": {
                        "decimal": "",
                        "emptyTable": "No data available in table",
                        "info": "",
                        "infoEmpty": "",
                        "infoFiltered": "",
                        "infoPostFix": "",
                        "thousands": ",",
                        "lengthMenu": "Prikaži _MENU_ zapisa",
                        "loadingRecords": "Učitavajne...",
                        "processing": "Processing...",
                        "search": "Pretraga:",
                        "zeroRecords": "Nema rezultata pretragi",
                        "paginate": {
                          "first": "Prvi",
                          "last": "Drugi",
                          "next": "Slijedeći",
                          "previous": "Prethodni"
                        },
                        "aria": {
                          "sortAscending": ": activate to sort column ascending",
                          "sortDescending": ": activate to sort column descending"
                        }
                      },
                      

                   })
                   
                   
                } else {
                    if (errcode == 999) {
                        $("#container").html(loginForm);
                    } else {
                        Swal.fire(message + '.' + errcode);
                    }
                }

        
            

        },
        error: function (xhr, textStatus, error) {
            console.log(xhr.statusText);
            console.log(textStatus);
            console.log(error);
        },
        async: false

    })
  
}

function odabrana(ID_komer) {
    var tablicatt = '<div class="editTbl" ><table class="table table-hover"><tbody>';
    $.ajax({
        type: 'POST',
        url: url,
        data: {
            "projekt": "p_komercijalna",
            "procedura": "p_get_teme",
            "ID": ID_komer
        },

        success: function (data) {
            var jsonBody = JSON.parse(data);
            var errcode = jsonBody.h_errcode;
            var message = jsonBody.h_message;

            if (message == null || message == "", errcode == null || errcode == 0) {
                $.each(jsonBody.data, function (k, v) {

                    tablicatt = "<div  id='container'><div  id='podaci'></div>";
                    tablicatt += "<div class='centar'>";
                    tablicatt += "<form class='form-container-ob' name='myForm' method='post' onSubmit='return false;'>";
                    tablicatt +='<p class="obavijest"> Vaša odabrana tema je: <br><br>' + v.NAZIV + '<p>'
                    tablicatt += '<input class="nestani" type="text" id="IDteme" value="' + v.ID + '" readonly>' //IDteme
                
                    
                    tablicatt += "<button  onClick='logout()' title='Logout' class='logout' ><i class='fa-solid fa-right-from-bracket'></i>  Logout</button></form></div></div>";
         
                });
                $("#container").html(tablicatt);
  
            } else {
                if (errcode == 999) {
                    $("#container").html(loginForm);
                } else {
                    Swal.fire(message + '.' + errcode);
                }
            }
          // refresh();
        },
        error: function (xhr, textStatus, error) {
            console.log(xhr.statusText);
            console.log(textStatus);
            console.log(error);
        },
        async: true

    });
}

function inicijalizacijaUc() {
    
            return "<div class='flex'><p class='naziv'>Odaberite temu</p></div><div class='tbl_ucenik '><div class='por'><table id='example' class='display table-hover cell-border ' style='width:90%'><thead><tr>"+
            
                   " <th>NAZIV</th>"+
                   " <th>PROFESOR</th>"+
                   "<th>PREDMET</th><th></th></tr></thead></table></div> </div><div class='flex'><button  onClick='logout()' class='logout_ucenik' ><i class='fa-solid fa-right-from-bracket'></i>  Logout</button></div><div  class='copyu'><img class='vub' src='VUB_logo_png.png' ><p >Veleučilište u Bjelovaru ©</p></div> ";
       
    }


    


function testt(ID_tema) {
    var tablicat = '<div class="editTbl" ><table class="table table-hover"><tbody>';
    $.ajax({
        type: 'POST',
        url: url,
        data: {
            "projekt": "p_komercijalna",
            "procedura": "p_get_teme",
            "ID": ID_tema
        },

        success: function (data) {
            var jsonBody = JSON.parse(data);
            var errcode = jsonBody.h_errcode;
            var message = jsonBody.h_message;

            if (message == null || message == "", errcode == null || errcode == 0) {
                $.each(jsonBody.data, function (k, v) {

                    tablicat = "<div  id='container'>";
                    tablicat += "<div class='form-popup'>";
                    tablicat += "<form class='form-container-ob' name='myForm' method='post' onSubmit='return false;'>";
                    tablicat +='<p class="obavijest"> Jeste li sigurni da želite odabrati temu: <br><br> ' + v.NAZIV + ' ?<p>'
                    tablicat += '<input class="nestani" type="text" id="IDteme" value="' + v.ID + '" readonly>' //IDteme

                    tablicat += "<button class='btn' id='spremiOdabir' ><i class='fa-solid fa-floppy-disk'></i> Da, odaberi</button><br><button class='cancel' id='odustani' onClick='ucenik()'><i class='fa-solid fa-ban'></i></i> Odustani</button></form></div></div>";
         
                });
                $("#container").html(tablicat);
  
            } else {
                if (errcode == 999) {
                    $("#container").html(loginForm);
              
                } else {
                    Swal.fire(message + '.' + errcode);
                }
            }
           // refresh();
        },
        error: function (xhr, textStatus, error) {
            console.log(xhr.statusText);
            console.log(textStatus);
            console.log(error);
        },
        async: true

    });
}

$(document).on('click', '#spremiOdabir', function () {
    var TEME_ID = $('#IDteme').val();

    //console.log(TEME_ID, 'IDTEME');
   // alert("eeee");
    //var UCENIK_ID = $('#IDucenika').val();
    //var ID_odabira = $('#ID').val();
   
        $.ajax({
            type: 'POST',
            url: url,
            data: {
                "projekt": "p_komercijalna",
                "procedura": "p_save_odabir",
                "IDteme": TEME_ID
            },
            success: function (data) {
          
                var jsonBody = JSON.parse(data);
                var errcode = jsonBody.h_errcode;
                var message = jsonBody.h_message;
         
             
                
                if ((message == null || message == "") && (errcode == null || errcode == 0)) {
                    $("#container").html(odabrana);
                    Swal.fire('Uspješno ste odabrali temu');
                    
                } else {
                    Swal.fire(message);
                    console.log(errcode);
                    $("#container").html(odabrana);
                }

            },
            
            error: function (xhr, textStatus, error) {
           
                console.log(xhr.statusText);
                console.log(textStatus);
                console.log(error);
            },
            async: true
        
        });
       
    
})

