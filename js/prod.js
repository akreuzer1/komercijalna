var inDataProd = { "projekt": "p_komercijalna", "procedura": "p_get_ucenicip" };

function prod() {
    $.ajax({
        type: 'POST',
        url: url,
        data: inDataProd,
        success: function (data) {
            var jsonBody = JSON.parse(data);
            var errcode = jsonBody.h_errcode;
            var message = jsonBody.h_message;
            var count = jsonBody.count;

            var izlaz = inicijalizacijaProd();

            if (message == null || message == "", errcode == null || errcode == 0) {

                 

 
               
                   document.getElementById("container").innerHTML = izlaz; 
                   $("#example").DataTable({
                       data: jsonBody.data,
                       "columns":[
                        { "data": "UCENIK" },
                        { "data": "RAZRED" },
                        { "data": "MENTOR" },
                        { "data": "PREDMET" },
                        { "data": "TEMA" },
                       
                        {
                            "data": "ACTION",
                            "autoWidth": false,
                            "orderable": false
                        },
                           
                        {
                            "data": "ACTIONN",
                            "autoWidth": false,
                            "orderable": false
                        }
                          
                       ],
                       "columnDefs": [
                        { "width": "20px", "targets": 5, "className": "dt-center" },
                        { "width": "20px", "targets": 6, "className": "dt-center" }
                      ],
                      "language": {
                        "decimal": "",
                        "emptyTable": "No data available in table",
                        "info": "",
                        "infoEmpty": "",
                        "infoFiltered": "",
                        "infoPostFix": "",
                        "thousands": ",",
                        "lengthMenu": "Prikaži _MENU_ zapisa",
                        "loadingRecords": "Učitavajne...",
                        "processing": "Processing...",
                        "search": "Pretraga:",
                        "zeroRecords": "Nema rezultata pretragi",
                        "paginate": {
                            "first": "Prvi",
                            "last": "Drugi",
                            "next": "Slijedeći",
                            "previous": "Prethodni"
                        },
                        "aria": {
                            "sortAscending": ": activate to sort column ascending",
                            "sortDescending": ": activate to sort column descending"
                        }
                    }

                   })
                  
                   
                } else {
                    if (errcode == 999) {
                        $("#container").html(loginForm);
                    } else {
                        Swal.fire(message + '.' + errcode);
                    }
                }
            

        },
        error: function (xhr, textStatus, error) {
            console.log(xhr.statusText);
            console.log(textStatus);
            console.log(error);
        },
        async: false

    })

}

function inicijalizacijaProd() {
    
            return "<div class='content'><div class='flex'><p class='naziv'>Prodavači</p></div><div class='tbl'><div class='por'><table id='example' class='display table-hover cell-border ' style='width:100%'><thead><tr>"+
                   //" <th>ID</th>"+
                   " <th>UČENIK</th>"+
                   " <th>RAZRED</th>"+
                   " <th>MENTOR</th>"+
                   " <th>PREDMET</th>"+
                   "<th>TEMA</th><th></th><th></th></tr></thead></table></div><button class='unesi_prof' onClick='insertKomer()'><i class='fa-solid fa-download'></i>  Unesi učenika</button><button class='mail'><i class='fa-sharp fa-solid fa-envelope-open-text'></i> Pošalji na mail</button> </div></div>";
       
    }

    function delUcenikp(ID_prodavac) {

        Swal.fire({
            // title: 'Želite li zaista obrisati predmet?',
            title: "<h5 style='color:black'>" + 'Želite li zaista obrisati učenika?' + "</h5>",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#2E2D7A',
            background: '#CBCBCB',
            cancelButtonColor: '#ED1C24',
            confirmButtonText: 'Da, obriši učenika!',
            cancelButtonText: 'Odustani!',
            animation: true,
    
        }).then((result) => {
            if (result.isConfirmed) {
                $.ajax({
                    type: 'POST',
                    url: url,
                    data: {
                        "projekt": "p_komercijalna",
                        "ID": ID_prodavac,
                        "procedura": "p_obrisi_ucenika"
                   
                    },
    
                    success: function (data) {
                        var jsonBody = JSON.parse(data);
                        var errcode = jsonBody.h_errcode;
                        var message = jsonBody.h_message;
                        console.log(data);
                        console.log(ID_predmet);
    
                        if ((message == null || message == "") && (errcode == null || errcode == 0)) {
                            Swal.fire(
    
                                'Uspješno ',
                                'ste obrisali učenika',
                                'success'
                            );
                        } else {
                            Swal.fire(message + '.' + errcode);
                        }
                        refresh();
                        prod();
    
                    },
                    error: function (xhr, textStatus, error) {
                        console.log(xhr.statusText);
                        console.log(textStatus);
                        console.log(error);
                    },
                    async: true
                });
            }
        })
    }
    
    
    //---------------- Edit predmeta ---------------
    
    function editUcenikp(ID_prodavac) {
        var tablicaP = '<div class="editTbl" ><table class="table table-hover"><tbody>';
        $.ajax({
            type: 'POST',
            url: url,
            data: {
                "projekt": "p_komercijalna",
                "procedura": "p_get_ucenicip",
                "ID": ID_prodavac
            },
    
            success: function (data) {
                var jsonBody = JSON.parse(data);
                var errcode = jsonBody.h_errcode;
                var message = jsonBody.h_message;
    
                if (message == null || message == "", errcode == null || errcode == 0) {
                    $.each(jsonBody.data, function (k, v) {
                       
                        tablicaP = "<div class='content' id='container'>";
                        tablicaP += "<div class='form-popup'>";
                        tablicaP += "<form class='form-container' name='myForm' method='post'>";
                        tablicaP += '<input class="nestani" type="text" id="ID" value="' + v.ID + '" readonly></input>'
                        tablicaP += "<label><b>Učenik</b></label>";
                        tablicaP += '<input type="text" value="' + v.UCENIK + '" id="ucenik" required> ';
                        tablicaP += "<label><b>Razred</b></label>";
                        tablicaP += '<input type="text" value="' + v.RAZRED + '" id="razred" required> ';
                        tablicaP += "<label><b>Mentor</b></label>";
                        tablicaP += '<input type="text" value="' + v.MENTOR + '" id="mentor" required> ';
                        tablicaP += "<label><b>Ime predmeta</b></label>";
                        tablicaP += '<input type="text" value="' + v.PREDMET + '" id="predmet" required> ';
                        tablicaP += "<label><b>Ime teme</b></label>";
                        tablicaP += '<input type="text" value="' + v.TEMA + '" id="tema" required> ';
                        tablicaP += "<button class='btn' id='spremiProd'><i class='fa-solid fa-floppy-disk'></i> Unesi</button><button class='cancel' id='odustani' onClick='prod()'><i class='fa-solid fa-ban'></i></i> Odustani</button></form></div></div>";
             
                    });
                    $("#container").html(tablicaP);
                } else {
                    if (errcode == 999) {
                        $("#container").html(loginForm);
                    } else {
                        Swal.fire(message + '.' + errcode);
                    }
                }
                //refresh();
            },
            error: function (xhr, textStatus, error) {
                console.log(xhr.statusText);
                console.log(textStatus);
                console.log(error);
            },
            async: true
    
        });
    }
    