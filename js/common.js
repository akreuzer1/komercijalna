//------konstante------------------------------------------------
var url = "https://dev.vub.zone/zavrsne/router.php";
var projekt = "p_common";
var perPage = 10;

//------hendlanje link button-a----------------------------------

window.onload = function () {
  $("#container").html(loginForm);

};

$("#getLogin").click(function () {
  $("#menu").html(generateMenu);
});

$("#pred").click(function () {
  showSVE();
});
$("#prikazz").click(function () {
  dohprof();
});
$("#logoutBtn").click(function () {
  logout();
});

//------------refersh-------------------------------------------------
$(function () {
  refresh();
  loginForm();
  
});

//------------refersh-------------------------------------------------

function refresh() {
  $.ajax({
    type: "POST",
    url: url,
    data: { projekt: "p_common", procedura: "p_refresh" },
    success: function (data) {
    console.log(data);
      var jsonBody = JSON.parse(data);
      if (jsonBody.h_errcode !== 999) {
          var podaci = '<p class="info"> Ime Prezime: ' + jsonBody.ime + ' ' + jsonBody.prezime +'</p>';
         $("#podaci").html(podaci);
        if (jsonBody.ovlast == 0) {
          $("#menu").html(generateMenu);
          $("#container").html(komer);
         
        } else if (jsonBody.ovlast == 1) {
          $("#menu").html(generateMenuProf);
          $("#container").html(ekran_prof);

        } else if (jsonBody.ovlast == 2 && jsonBody.odabir == 1 ) {
          
          $("#container").html(odabrana);
        }
        else{
          $("#container").html(ucenik);

        }
      
      }
    },
    error: function (xhr, textStatus, error) {
      console.log(xhr.statusText);
      console.log(textStatus);
      console.log(error);
    },
    async: true,
  });
}

//----------Meni-------------

function generateMenu() {
  var meni = "<ul class='sidenav'>";
  meni += "<div class='sidebar-head'>";
  meni += "<img src='logo.png' href='http://www.trgovackaskola-bjelovar.hr/' alt='logo1' class='logo1'></div>";
  meni += "<li><a  onClick='komer()'>Komercijalisti</a></li>";
  meni += "<li><a  onClick='prod()' id='uvidBtn'>Prodavači</a></li>";
  meni += "<li><a  onClick='prof()'>Profesori</a></li>";
  meni += "<li><a  onClick='teme()'>Teme</a></li>";
  meni += "<li><a  onClick='predmeti()' id='pred' >Predmeti</a></li>";
  meni += "<li><div class='flex'><button  onClick='logout()' title='Logout' class='logout' ><i class='fa-solid fa-right-from-bracket'></i>  Logout</button></div><div  class='copy flex'><img class='vub' src='VUB_logo_png.png' ><p class='copyp flex' >Veleučilište u Bjelovaru ©</p></div></li>";

  $("#menu").html(meni);
}
function generateMenuProf() {
  var menip = "<ul class='sidenav'>";
  menip += "<div class='sidebar-head'>";
  menip += "<img src='logo.png' href='http://www.trgovackaskola-bjelovar.hr/' alt='logo1' class='logo1'></div>";
  menip +="<li><a  onClick='ekran_prof()'><i class='fa-solid fa-graduation-cap ikone'></i> Učenici</a></li>";
  menip +="<li><a  onClick='teme_prof()'><i class='fa-solid fa-user-tie ikone'></i> Teme</a></li>";
  menip +="<li><div class='flex'><button  onClick='logout()' title='Logout' class='logout' ><i class='fa-solid fa-right-from-bracket'></i>  Logout</button></div><div  class='copyp flex'><img class='vub' src='VUB_logo_png.png' ><p >Veleučilište u Bjelovaru ©</p></div></li>";

  $("#menu").html(menip);
}

//-----------ajaxSetup-------------------------------------------------------
$.ajaxSetup({
  xhrFields: {
    withCredentials: true,
  },
});
