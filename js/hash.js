
  var url_string = window.location.href; // www.test.com?filename=test
  var url1 = new URL(url_string);
  var paramValue = url1.searchParams.get("hash");
  
  var url2 = "https://dev.vub.zone/zavrsne/router.php";
  $.ajax({
      type: 'POST',
      url: url2,
      data: {"projekt": "p_komercijalna", 
             "procedura": "p_login", 
             "hash": paramValue
          },
      success: function (data) {
          var jsonBody = JSON.parse(data);
          var errcod = jsonBody.h_errcod;
          var message = jsonBody.h_message;

          if (message == null || message == "", errcod == null || errcod == 0) {
              $("#container").html('');
              refresh();
          } else {
              Swal.fire(message + '.' + errcod);
      
          }
          
      },
      error: function (xhr, textStatus, error) {
          console.log(xhr.statusText);
          console.log(textStatus);
          console.log(error);
      },
      async: true

  });