var inDataK = { projekt: "p_komercijalna", procedura: "p_get_ucenicik" };

function komer() {
  $.ajax({
    type: "POST",
    url: url,
    data: inDataK,
    success: function (data) {
      var jsonBody = JSON.parse(data);
      var errcode = jsonBody.h_errcode;
      var message = jsonBody.h_message;
      var count = jsonBody.count;

      var izlazK = inicijalizacijaKK();

      if ((message == null || message == "", errcode == null || errcode == 0)) {
        document.getElementById("container").innerHTML = izlazK;

        $("#example").DataTable({
          data: jsonBody.data,
          columns: [
            { data: "UCENIK" },
            { data: "RAZRED" },
            { data: "MENTOR" },
            { data: "PREDMET" },
            { data: "TEMA" },

            {
              data: "ACTION",
              autoWidth: false,
              orderable: false,
            },

            {
              data: "ACTIONN",
              autoWidth: false,
              orderable: false,
            },
          ],
          columnDefs: [
            { width: "20px", targets: 5, className: "dt-center" },
            { width: "20px", targets: 6, className: "dt-center" },
          ],
          language: {
            decimal: "",
            emptyTable: "No data available in table",
            info: "",
            infoEmpty: "",
            infoFiltered: "",
            infoPostFix: "",
            thousands: ",",
            lengthMenu: "Prikaži _MENU_ zapisa",
            loadingRecords: "Učitavajne...",
            processing: "Processing...",
            search: "Pretraga:",
            zeroRecords: "Nema rezultata pretragi",
            paginate: {
              first: "Prvi",
              last: "Drugi",
              next: "Slijedeći",
              previous: "Prethodni",
            },
            aria: {
              sortAscending: ": activate to sort column ascending",
              sortDescending: ": activate to sort column descending",
            },
          },
        });
      } else {
        if (errcode == 999) {
          $("#container").html(loginForm);
        } else {
          Swal.fire(message + "." + errcode);
        }
      }
    },
    error: function (xhr, textStatus, error) {
      console.log(xhr.statusText);
      console.log(textStatus);
      console.log(error);
    },
    async: false,
  });
}

function inicijalizacijaKK() {
  return (
    "<div class='content'></i><div class='flex'><p class='naziv'>Komercijalisti</p></div><div class='tbl'><div class='por'><table id='example' class='display table-hover cell-border ' style='width:100%'><thead><tr>" +
    "<tr><th>UČENIK</th>" +
    " <th>RAZRED</th>" +
    " <th>MENTOR</th>" +
    " <th>PREDMET</th>" +
    "<th>TEMA</th><th ></th><th></th></thead></table></div><button class='unesi_prof' title='Unesi učenika' onClick='insertKomer()'><i class='fa-solid fa-download'></i>  Unesi učenika</button><button class='mail'><i class='fa-sharp fa-solid fa-envelope-open-text'></i> Pošalji na mail</button> </div> <div class='flex'><button  onClick='logout()' class='logout mm' ><i class='fa-solid fa-right-from-bracket'></i>  Logout</button></div>"
  );
}

//--------forma za unos učenika
function insertKomer() {
  var unos = "<div class='content' id='container'>";
  unos += "<div class='form-popup'>";
  unos += "<form class='form-container' name='myForm' method='post' onSubmit='return false;'>";
  unos += "<label><b>Ime</b></label>";
  unos += "<input type='text' placeholder='Ime' name='ime' id='ime' required> ";
  unos += "<label><b>Prezime</b></label>";
  unos += "<input type='text' placeholder='Prezime' name='prezime' id='prezime' required>";
  unos += "<label><b>E-mail</b></label>";
  unos += "<input type='email' placeholder='E-mail' name='mail' id='email' required>";
  unos += "<label><b>Lozinka</b></label>";
  unos += "<input type='password' placeholder='Lozinka' name='pass' id='lozinka' required>";
  unos += "<label><b>Smjer</b></label><br>";
  unos += "<select class='drop' id='smjer'>";
  unos += "<option value='2'>Komerijalisti</option>";
  unos += "<option value='1'>Prodavači</option>";
  unos += "</select> <br>";
  unos += "<label><b>Razred</b></label><br>";
  unos += "<select class='drop' id='IDrazred'>";
  unos += "<option value='4'>4.a</option>";
  unos += "<option value='5'>4.b</option>";
  unos += "<option value='6'>4.c</option>";
  unos += "<option value='1'>3.a</option>";
  unos += "<option value='2'>3.b</option>";
  unos += "<option value='3'>3.c</option>";
  unos += "</select>";
  unos +="<button class='btn' id='spremiKomer'><i class='fa-solid fa-floppy-disk'></i> Unesi</button><button class='cancel' id='odustani' onClick='komer()'><i class='fa-solid fa-ban'></i></i> Odustani</button></form></div></div>";
  $("#container").html(unos);

  

  $(document).ready(function() {
    $('#drop').select2();
    $('select').select2({  

        language: {
      
          noResults: function() {
      
            return "Nema rezultata";        
          },
          searching: function() {
      
            return "Tražim..";
          }
        }
      });

});


}

$(document).on("click", "#spremiKomer", function () {
  var IME_komer = $("#ime").val();
  var PREZIME_komer = $("#prezime").val();
  var EMAIL_komer = $("#email").val();
  var ZAPORKA_komer = $("#lozinka").val();
  var SMJER_komer = $("#smjer").val();
  var RAZRED_komer = $("#IDrazred").val();
  var ID_komer = $("#ID").val();

  if (IME_komer == null || IME_komer == "") {
    Swal.fire("Molimo unesite ime ucenika");
  } else if (PREZIME_komer == null || PREZIME_komer == "") {
    Swal.fire("Molimo unesite prezime ucenika");
  } else if (EMAIL_komer == null || EMAIL_komer == "") {
    Swal.fire("Molimo unesite email ucenika");
  } else if (SMJER_komer == null || SMJER_komer == "") {
    Swal.fire("Molimo unesite smjer ucenika");
  } else if (RAZRED_komer == null || RAZRED_komer == "") {
    Swal.fire("Molimo unesite razred ucenika");
  } else if (
    (ZAPORKA_komer == null || ZAPORKA_komer == "") &&
    (ID_komer == null || ID_komer == "")
  ) {
    Swal.fire("Molimo unesite zaporku ucenika");
  } else {
    $.ajax({
      type: "POST",
      url: url,
      data: {
        projekt: "p_komercijalna",
        procedura: "p_save_ucenici",
        ID: ID_komer,
        IME: IME_komer,
        PREZIME: PREZIME_komer,
        EMAIL: EMAIL_komer,
        ZAPORKA: ZAPORKA_komer,
        SMJER: SMJER_komer,
        IDRAZRED: RAZRED_komer,
      },
      success: function (data) {
        console.log(data)
        var jsonBody = JSON.parse(data);
        var errcode = jsonBody.h_errcode;
        var message = jsonBody.h_message;
        console.log(data);

        if (
          (message == null || message == "") &&
          (errcode == null || errcode == 0)
        ) {
          Swal.fire("Uspješno se unijeli korisnika");
        } else {
          Swal.fire(message + "." + errcode);
        }
        refresh();
      },
      error: function (xhr, textStatus, error) {
        console.log(xhr.statusText);
        console.log(textStatus);
        console.log(error);
      },
      async: true,
    });
  }
});

function delUcenikk(ID_komer) {
  Swal.fire({
    title: "<h5 style='color:black'>" + 'Želite li zaista obrisati učenika?' + "</h5>",
    icon: "warning",
    showCancelButton: true,
    confirmButtonColor: '#2E2D7A',
    background: '#CBCBCB',
    cancelButtonColor: '#ED1C24',
    confirmButtonText: "Da, obriši učenika!",
    cancelButtonText: "Ipak nemoj!",
    animation: true,
  }).then((result) => {
    if (result.isConfirmed) {
      $.ajax({
        type: "POST",
        url: url,
        data: {
          projekt: "p_komercijalna",
          procedura: "p_obrisi_ucenika",
          ID: ID_komer,
        },
        success: function (data) {
          var jsonBody = JSON.parse(data);
          var errcode = jsonBody.h_errcode;
          var message = jsonBody.h_message;
          console.log(data);

          if (
            (message == null || message == "") &&
            (errcode == null || errcode == 0)
          ) {
            Swal.fire("Uspješno ", "ste obrisali učenika", "success");
          } else {
            Swal.fire(message + "." + errcode);
          }
          refresh();
          komer();
        },
        error: function (xhr, textStatus, error) {
          console.log(xhr.statusText);
          console.log(textStatus);
          console.log(error);
        },
        async: true,
      });
    }
  });
}

//---------------- Edit predmeta ---------------

function editUcenikk(ID_komer) {
  var tablicaK =
    '<div class="editTbl" ><table class="table table-hover"><tbody>';
  $.ajax({
    type: "POST",
    url: url,
    data: {
      projekt: "p_komercijalna",
      procedura: "p_get_ucenicik",
      ID: ID_komer,
    },

    success: function (data) {
      var jsonBody = JSON.parse(data);
      var errcode = jsonBody.h_errcode;
      var message = jsonBody.h_message;

      if ((message == null || message == "", errcode == null || errcode == 0)) {
        $.each(jsonBody.data, function (k, v) {
          tablicaK = "<div class='content' id='container'>";
          tablicaK += "<div class='form-popup'>";
          tablicaK +=
            "<form class='form-container' name='myForm' method='post'>";
          tablicaK +=
            '<input class="nestani" type="text" id="ID" value="' +
            v.ID +
            '" readonly></input>';
          tablicaK += "<label><b>Učenik</b></label>";
          tablicaK +=
            '<input type="text" value="' +
            v.UCENIK +
            '" id="ucenik" required> ';
          tablicaK += "<label><b>Razred</b></label>";
          tablicaK +=
            '<input type="text" value="' +
            v.RAZRED +
            '" id="razred" required> ';
          tablicaK += "<label><b>Mentor</b></label>";
          tablicaK +=
            '<input type="text" value="' +
            v.MENTOR +
            '" id="mentor" required> ';
          tablicaK += "<label><b>Ime predmeta</b></label>";
          tablicaK +=
            '<input type="text" value="' +
            v.PREDMET +
            '" id="predmet" required> ';
          tablicaK += "<label><b>Ime teme</b></label>";
          tablicaK +=
            '<input type="text" value="' + v.TEMA + '" id="tema" required> ';
          tablicaK +=
            "<button class='btn' id='spremiKomer'><i class='fa-solid fa-floppy-disk'></i> Unesi</button><button class='cancel' id='odustani' onClick='komer()'><i class='fa-solid fa-ban'></i></i> Odustani</button></form></div></div>";
        });
        $("#container").html(tablicaK);
      } else {
        if (errcode == 999) {
          $("#container").html(loginForm);
        } else {
          Swal.fire(message + "." + errcode);
        }
      }
      //  refresh();
    },
    error: function (xhr, textStatus, error) {
      console.log(xhr.statusText);
      console.log(textStatus);
      console.log(error);
    },
    async: true,
  });
}
