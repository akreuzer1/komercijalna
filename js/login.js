function loginForm() {
  var loginForma = "<div class='sidebar-head'>";
  loginForma += "<img src='logo.png' alt='logo' class='logo'></div>";
  loginForma += "<div class='form-popup'>";
  loginForma +=
    "<form class='form-container-login' name='myForm' method='post'>";
  loginForma += "<label><b>E-mail</b></label>";
  loginForma +=
    "<input type='email' placeholder='E-mail' name='mail' id='inputEmail' required>";
  loginForma += "<label><b>Lozinka</b></label>";
  loginForma +=
    "<input type='password' placeholder='Lozinka' name='pass' id='inputPassword' required>";
  loginForma +=
    "<button class='btn' id='getLogin'><i class='fa-solid fa-user-plus'></i> Prijavi se</button></form></div> <div class='flex'><div class='copyl'><img class='vub' src='VUB_logo_png.png' ><p>Veleučilište u Bjelovaru ©</p></div></div>";

  return loginForma;
}

//kontrole za unos podataka
//------------------------LOGIN--------------------------------
$(document).on("click", "#getLogin", function () {
  var email = $("#inputEmail").val();
  var password = $("#inputPassword").val();
  if (email == null || email == "") {
    Swal.fire("Molimo unesite email adresu");
  } else if (password == null || password == "") {
    Swal.fire("Molimo unesite zaporku");
  } else {
    login();
  }
});

function login() {
  $.ajax({
    type: "POST",
    url: url,
    data: {
      projekt: "p_komercijalna",
      procedura: "p_login",
      username: $("#inputEmail").val(),
      password: $("#inputPassword").val(),
    },
    success: function (data) {
      var jsonBody = JSON.parse(data);
      var errcod = jsonBody.h_errcod;
      var message = jsonBody.h_message;

      if ((message == null || message == "", errcod == null || errcod == 0)) {
        $("#container").html("");
        refresh();
      } else {
        Swal.fire({
            title: "<h5 style='color:black'>" + message + "</h5>",
            confirmButtonColor: '#2E2D7A',
            background: '#CBCBCB',
            confirmButtonText: "OK",
            animation: true,
            
          });
      }
    },
    error: function (xhr, textStatus, error) {
      console.log(xhr.statusText);
      console.log(textStatus);
      console.log(error);
    },
    async: true,
  });
}

/*

//kontrole za unos podataka
//------------------------LOGIN--------------------------------
$(document).on('click', '#getLogin', function () {
    var email = $('#inputEmail').val();
    var password = $('#inputPassword').val();
    if (email == null || email == "") {
        Swal.fire('Molimo unesite email adresu');
    } else if (password == null || password == "") {
        Swal.fire('Molimo unesite zaporku');
    } else {
        login();
    }
})

function login() {
    $.ajax({
        type: 'POST',
        url: url,
        data: {"projekt": "p_komercijalna", 
               "procedura": "p_login", 
               "username": $('#inputEmail').val(), 
               "password": $('#inputPassword').val()
            },
        success: function (data) {
            var jsonBody = JSON.parse(data);
            var errcod = jsonBody.h_errcod;
            var message = jsonBody.h_message;

            if (message == null || message == "", errcod == null || errcod == 0) {
                $("#container").html('');
                refresh();
            } else {
                Swal.fire(message + '.' + errcod);
        
            }
            
        },
        error: function (xhr, textStatus, error) {
            console.log(xhr.statusText);
            console.log(textStatus);
            console.log(error);
        },
        async: true

    });
}*/

function logout() {

  $.ajax({
    type: "POST",
    url: url,
    data: { projekt: "p_common", procedura: "p_logout" },
    success: function (data) {
      
      var jsonBody = JSON.parse(data);
      var errcode = jsonBody.h_errcode;
      var message = jsonBody.h_message;

      if (message == null || message == "" || errcode == null) {
        Swal.fire("Greška u obradi podataka, molimo pokušajte ponovno!");
      } else {
   
        Swal.fire({
          title: "<h5 style='color:black'>" + message + "</h5>",
          confirmButtonColor: '#2E2D7A',
          background: '#CBCBCB',
          color: '#fff',
          confirmButtonText: "OK",
          animation: true,
          
        });
      }
      $("#container").html("");
      $("#podaci").html("");
      $("#menu").html("");
      $("#container").html(loginForm);
    },
    error: function (xhr, textStatus, error) {
      console.log(xhr.statusText);
      console.log(textStatus);
      console.log(error);
    
    },
    async: true,
  });
}
