
var inDataProff = { projekt: "p_komercijalna", procedura: "p_get_profesori" };

function prof() {
  $.ajax({
    type: "POST",
    url: url,
    data: inDataProff,
    success: function (data) {
      var jsonBody = JSON.parse(data);
      var errcode = jsonBody.h_errcode;
      var message = jsonBody.h_message;
      var count = jsonBody.count;

      var izlazp = inicijalizacijaProf();

      if ((message == null || message == "", errcode == null || errcode == 0)) {
        document.getElementById("container").innerHTML = izlazp;
        $("#example").DataTable({
          data: jsonBody.data,
          columns: [
            { data: "PROFESOR" },
            { data: "EMAIL" },
         
            {
              data: "ACTION",
              autoWidth: false,
              orderable: false,
            },

            {
              data: "ACTIONN",
              autoWidth: false,
              orderable: false,
            }
          ],
          columnDefs: [
            { width: "20px", targets: 2, className: "dt-center" },
            { width: "20px", targets: 3, className: "dt-center" },
          ],
          language: {
            decimal: "",
            emptyTable: "No data available in table",
            info: "",
            infoEmpty: "",
            infoFiltered: "",
            infoPostFix: "",
            thousands: ",",
            lengthMenu: "Prikaži _MENU_ zapisa",
            loadingRecords: "Učitavajne...",
            processing: "Processing...",
            search: "Pretraga:",
            zeroRecords: "Nema rezultata pretragi",
            paginate: {
              first: "Prvi",
              last: "Drugi",
              next: "Slijedeći",
              previous: "Prethodni",
            },
            aria: {
              sortAscending: ": activate to sort column ascending",
              sortDescending: ": activate to sort column descending",
            },
          },
        });
      } else {
        if (errcode == 999) {
          $("#container").html(loginForm);
        } else {
          Swal.fire(message + "." + errcode);
        }
      }
    },
    error: function (xhr, textStatus, error) {
      console.log(xhr.statusText);
      console.log(textStatus);
      console.log(error);
    },
    async: false,
  });
}

function inicijalizacijaProf() {
  return (
    "<div class='content'><div class='flex'><p class='naziv'>Profesori</p></div><div class='tbl'><div class='por'><table id='example' class='display table-hover cell-border ' style='width:100%'><thead><tr>" +
    " <th>PROFESOR</th>" +
    " <th>EMAIL</th>" +
    "<th></th><th></th></tr></thead></table></div><button class='unesi_prof' onClick='insertProf()'><i class='fa-solid fa-download'></i>  Unesi profesora</button>  </div></div>"
  );
}

//--------forma za unos profesora
function insertProf() {
  var unos = "<div class='content' id='container'>";
  unos += "<div class='form-popup'>";
  unos += "<form class='form-container' name='myForm' method='post'>";
  unos += "<label><b>Ime</b></label>";
  unos += "<input type='text' placeholder='Ime' id='ime' required> ";
  unos += "<label><b>Prezime</b></label>";
  unos += "<input type='text' placeholder='Prezime'  id='prezime' required>";
  unos += "<label><b>E-mail</b></label>";
  unos += "<input type='email' placeholder='E-mail'id='email' required>";
  unos += "<label><b>Lozinka</b></label>";
  unos += "<input type='password' placeholder='Lozinka' id='lozinka' required>";
  unos +=
    "<button class='btn' id='spremiProf'><i class='fa-solid fa-floppy-disk'></i> Unesi</button><button class='cancel' id='odustani' onClick='prof()'><i class='fa-solid fa-ban'></i></i> Odustani</button></form></div></div>";
  $("#container").html(unos);
}

$(document).on("click", "#spremiProf", function () {
  var IME_prof = $("#ime").val();
  var PREZIME_prof = $("#prezime").val();
  var EMAIL_prof = $("#email").val();
  var ZAPORKA_prof = $("#lozinka").val();
  var ID_prof = $("#ID").val();

  if (IME_prof == null || IME_prof == "") {
    Swal.fire("Molimo unesite ime profesora");
  } else if (PREZIME_prof == null || PREZIME_prof == "") {
    Swal.fire("Molimo unesite prezime profesora");
  } else if (EMAIL_prof == null || EMAIL_prof == "") {
    Swal.fire("Molimo unesite email profesora");
  } else if (
    (ZAPORKA_prof == null || ZAPORKA_prof == "") &&
    (ID_prof == null || ID_prof == "")
  ) {
    Swal.fire("Molimo unesite zaporku profesora");
  } else {
    $.ajax({
      type: "POST",
      url: url,
      data: {
        projekt: "p_komercijalna",
        procedura: "p_save_profesori",
        ID: ID_prof,
        ime: IME_prof,
        prezime: PREZIME_prof,
        email: EMAIL_prof,
        lozinka: ZAPORKA_prof,
      },
      success: function (data) {
        var jsonBody = JSON.parse(data);
        var errcode = jsonBody.h_errcode;
        var message = jsonBody.h_message;
        console.log(data);

        if (
          (message == null || message == "") &&
          (errcode == null || errcode == 0)
        ) {
          Swal.fire("Uspješno se unijeli korisnika");
        } else {
          Swal.fire(message + "." + errcode);
        }
        refresh();
      },
      error: function (xhr, textStatus, error) {
        console.log(xhr.statusText);
        console.log(textStatus);
        console.log(error);
      },
      async: true,
    });
  }
});

function delProfesor(ID_profesor) {
  Swal.fire({
    // title: 'Želite li zaista obrisati predmet?',
    title:
      "<h5 style='color:black'>" +
      "Želite li zaista obrisati profesora?" +
      "</h5>",
    icon: "warning",
    showCancelButton: true,
    confirmButtonColor: '#2E2D7A',
    background: '#CBCBCB',
    cancelButtonColor: '#ED1C24',
    confirmButtonText: "Da, obriši predmet!",
    cancelButtonText: "Odustani!",
    animation: true,
  }).then((result) => {
    if (result.isConfirmed) {
      $.ajax({
        type: "POST",
        url: url,
        data: {
          projekt: "p_komercijalna",
          ID: ID_profesor,
          procedura: "p_obrisi_profesora",
        },

        success: function (data) {
          var jsonBody = JSON.parse(data);
          var errcode = jsonBody.h_errcode;
          var message = jsonBody.h_message;
          console.log(data);

          if (
            (message == null || message == "") &&
            (errcode == null || errcode == 0)
          ) {
            Swal.fire("Uspješno ", "ste obrisali profesora", "success");
          } else {
            Swal.fire(message + "." + errcode);
          }
          refresh();
          prod();
        },
        error: function (xhr, textStatus, error) {
          console.log(xhr.statusText);
          console.log(textStatus);
          console.log(error);
        },
        async: true,
      });
    }
  });
}

//---------------- Edit predmeta ---------------

function editProfesor(ID_profesora) {
  var tablicaProf =
    '<div class="editTbl" ><table class="table table-hover"><tbody>';
  $.ajax({
    type: "POST",
    url: url,
    data: {
      projekt: "p_komercijalna",
      procedura: "p_get_profesori",
      ID: ID_profesora,
    },

    success: function (data) {
      var jsonBody = JSON.parse(data);
      var errcode = jsonBody.h_errcode;
      var message = jsonBody.h_message;

      if ((message == null || message == "", errcode == null || errcode == 0)) {
        $.each(jsonBody.data, function (k, v) {
          tablicaProf = "<div class='content' id='container'>";
          tablicaProf += "<div class='form-popup'>";
          tablicaProf +=
            "<form class='form-container' name='myForm' method='post'>";
          tablicaProf +=
            '<input class="nestani" type="text" id="ID" value="' +
            v.ID +
            '" readonly></input>';
          tablicaProf += "<label><b>Profesor</b></label>";
          tablicaProf +=
            '<input type="text" value="' +
            v.PROFESOR +
            '" id="ucenik" required> ';
          tablicaProf += "<label><b>E-mail</b></label>";
          tablicaProf +=
            '<input type="text" value="' + v.EMAIL + '" id="razred" required> ';
          tablicaProf += "<label><b>Ime predmeta</b></label>";
          tablicaProf +=
            '<input type="text" value="' +
            v.PREDMET +
            '" id="predmet" required> ';
          tablicaProf +=
            "<button class='btn' id='spremiProf'><i class='fa-solid fa-floppy-disk'></i> Unesi</button><button class='cancel' id='odustani' onClick='prof()'><i class='fa-solid fa-ban'></i></i> Odustani</button></form></div></div>";
        });

        $("#container").html(tablicaProf);
      } else {
        if (errcode == 999) {
          $("#container").html(loginForm);
        } else {
          Swal.fire(message + "." + errcode);
        }
      }
      //refresh();
    },
    error: function (xhr, textStatus, error) {
      console.log(xhr.statusText);
      console.log(textStatus);
      console.log(error);
    },
    async: true,
  });
}
