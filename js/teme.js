
var inDataTemaa = { "projekt": "p_komercijalna", "procedura": "p_get_temeAdmin" };


function teme() {
    $.ajax({
        type: 'POST',
        url: url,
        data: inDataTemaa,
        success: function (data) {
            var jsonBody = JSON.parse(data);
            var errcode = jsonBody.h_errcode;
            var message = jsonBody.h_message;
            var count = jsonBody.count;

            var izlaz = inicijalizacijaTema();

            if (message == null || message == "", errcode == null || errcode == 0) {
               
                   document.getElementById("container").innerHTML = izlaz; 
                   $("#example").DataTable({
                    
                     
                       data: jsonBody.data,
                       "columns":[
                           {"data":"NAZIV"},
                           {"data":"PROFESOR"},
                           {"data":"PREDMET"},
                           {
                            "data": "ACTION",
                            "autoWidth": false,
                            "orderable": false
                           },
                           
                           {
                               "data": "ACTIONN",
                               "autoWidth": false,
                               "orderable": false
                           }
                          
                          
                       ],
                       "columnDefs": [
                        { "width": "20px", "targets": 3, "className": "dt-center" },
                        { "width": "20px", "targets": 4, "className": "dt-center" }
                      ],
                      "language": {
                        "decimal": "",
                        "emptyTable": "No data available in table",
                        "info": "",
                        "infoEmpty": "",
                        "infoFiltered": "",
                        "infoPostFix": "",
                        "thousands": ",",
                        "lengthMenu": "Prikaži _MENU_ zapisa",
                        "loadingRecords": "Učitavajne...",
                        "processing": "Processing...",
                        "search": "Pretraga:",
                        "zeroRecords": "Nema rezultata pretragi",
                        "paginate": {
                            "first": "Prvi",
                            "last": "Drugi",
                            "next": "Slijedeći",
                            "previous": "Prethodni"
                        },
                        "aria": {
                            "sortAscending": ": activate to sort column ascending",
                            "sortDescending": ": activate to sort column descending"
                        }
                    }

                   })
                  
                   
                } else {
                    if (errcode == 999) {
                        $("#container").html(loginForm);
                    } else {
                        Swal.fire(message + '.' + errcode);
                    }
                }
            

        },
        error: function (xhr, textStatus, error) {
            console.log(xhr.statusText);
            console.log(textStatus);
            console.log(error);
        },
        async: false

    })

}

function inicijalizacijaTema() {
    
            return "<div class='content'><div class='flex'><p class='naziv'>Teme</p></div><div class='tbl'><div class='por'><table id='example' class='display table-hover cell-border ' style='width:100%'><thead><tr>"+
                   " <th>NAZIV</th>"+
                   " <th>PROFESOR</th>"+
                   "<th>PREDMET</th><th></th><th></th></tr></thead></table></div><button class='unesi_prof' onClick='insertTema()'><i class='fa-solid fa-download'></i>  Unesi temu</button>  </div></div>";
       
    }

    
//--------forma za unos teme
function insertTema() {
    var unos = "<div class='content' id='container'>";
    unos += "<div class='form-popup'>";
    unos += "<form class='form-container' name='myForm' method='post' onSubmit='return false;'>";
    unos += "<label><b>Naziv</b></label>";
    unos += "<input type='text' placeholder='Naziv' id='naziv' required> "; 
    unos += "<label for='Predmet'><b>Profesor</b><br></label>";
    unos += "<select class='js-example-basic-single dropp' id='IDprofesora' placeholder='Unesite '>";
    unos += "<option disabled selected>Unos profesora</option>";
    unos += dohprof();
    unos += "</select><br>";
    unos += "<label for='Predmet'><b>Predmet</b><br></label>";
    unos += "<select class='js-example-basic-single drop' id='IDpredmeta' placeholder='Unesite '>";
    unos += "<option disabled selected>Unos Predmeta</option>";
    unos += dohpred();
    unos += "</select><br>";
    unos += "<label for='razred'><b>Razred</b><br></label>";
    unos += "<select class='drop' id='IDrazred'>";
    unos += "<option value='4'>4.a</option>";
    unos += "<option value='5'>4.b</option>";
    unos += "<option value='6'>4.c</option>";
    unos += "<option value='1'>3.a</option>";
    unos += "<option value='2'>3.b</option>";
    unos += "<option value='3'>3.c</option>";
    unos += "</select>";
    unos += "<button class='btn' id='spremiTemu'><i class='fa-solid fa-floppy-disk'></i> Unesi</button><button class='cancel' id='odustani' onClick='teme()'><i class='fa-solid fa-ban'></i></i> Odustani</button></form></div></div>";
    $("#container").html(unos);


    $(document).ready(function() {
        $('#drop').select2();
        $('select').select2({  

            language: {
          
              noResults: function() {
          
                return "Nema rezultata";        
              },
              searching: function() {
          
                return "Tražim..";
              }
            }
          });
 
    });

   
  }
  function dohprof(){
    var options;  
    $.ajax({
        type: 'POST',
        url: url,
        data: {
            "projekt": "p_komercijalna", 
            "procedura": "p_get_profesori"
        },
        async: false,

        success: function (data) {
            var jsonBody = JSON.parse(data);
            var errcode = jsonBody.h_errcode;
            var message = jsonBody.h_message;
            var count = jsonBody.count;
            var podaci = jsonBody.data;
        
            

            $.each(podaci, function (index, value) {
             
               
                options += '<option value="' + value.ID + '" >' + value.PROFESOR + ' </option>';

               // console.log(options);
                //return options;
            });

            //$('.profesor').append(options);
            }
        })
       
     
        return options;
    }
    function dohpred(){
        var options;
        $.ajax({
            type: 'POST',
            url: url,
            data: {
                "projekt": "p_komercijalna", 
                "procedura": "p_get_predmeti"
            },
            async: false,

            success: function (data) {
                var jsonBody = JSON.parse(data);
                var errcode = jsonBody.h_errcode;
                var message = jsonBody.h_message;
                var count = jsonBody.count;
                var podaci = jsonBody.data;



                $.each(podaci, function (index, value) {


                    options += '<option value="' + value.ID + '" >' + value.PREDMET + ' </option>';

                   // console.log(options);
                    //return options;
                });

                //$('.profesor').append(options);
                }
            })
            return options;
        }
      


$(document).on('click', '#spremiTemu', function () {
      var NAZIV_teme = $('#naziv').val();
      var PROFESOR_teme = $('#IDprofesora').val();
      var PREDMET_teme = $('#IDpredmeta').val();
      var RAZRED_teme = $('#IDrazred').val();
      var ID_teme = $("#ID").val();

  
      if (NAZIV_teme == null || NAZIV_teme == "") {
          Swal.fire('Molimo unesite ime teme');
      } else if (PROFESOR_teme == null || PROFESOR_teme == "") {
          Swal.fire('Molimo unesite ime profesora');
      } else if (PREDMET_teme == null || PREDMET_teme == "") {
          Swal.fire('Molimo unesite predmet');
      } else if( (RAZRED_teme == null || RAZRED_teme == "")&&(ID_teme == null || ID_teme == "")){
          Swal.fire('Molimo unesite razred');
      } else {
          $.ajax({
              type: 'POST',
              url: url,
              data: {
                  "projekt": "p_komercijalna",
                  "procedura": "p_save_temu",
                  "naziv": NAZIV_teme,
                  "ID": ID_teme,
                  "IDpredmeta": PREDMET_teme,
                  "IDprofesora": PROFESOR_teme,
                  "IDrazred": RAZRED_teme
              },
              success: function (data) {
                console.log(data)
                  var jsonBody = JSON.parse(data);
                  var errcode = jsonBody.h_errcode;
                  var message = jsonBody.h_message;
                  console.log(data);
  
                  if ((message == null || message == "") && (errcode == null || errcode == 0)) {
                      Swal.fire('Uspješno se unijeli korisnika');
                  } else {
                      Swal.fire(message + '.' + errcode);
                  }
                  refresh();
                 
              },
              error: function (xhr, textStatus, error) {
                  console.log(xhr.statusText);
                  console.log(textStatus);
                  console.log(error);
              },
              async: true
          });
      }
  })

  
function delTema(ID_tema) {

    Swal.fire({
        // title: 'Želite li zaista obrisati predmet?',
        title: "<h5 style='color:white'>" + 'Želite li zaista obrisati temu?' + "</h5>",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#2E2D7A',
        background: '#CBCBCB',
        cancelButtonColor: '#ED1C24',
        confirmButtonText: 'Da, obriši temu!',
        cancelButtonText: 'Odustani!'

    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                type: 'POST',
                url: url,
                data: {
                    "projekt": "p_komercijalna",
                    "ID": ID_tema,
                    "procedura": "p_obrisi_temu"
               
                },

                success: function (data) {
                    var jsonBody = JSON.parse(data);
                    var errcode = jsonBody.h_errcode;
                    var message = jsonBody.h_message;
                    console.log(data);
                    console.log(ID_predmet);

                    if ((message == null || message == "") && (errcode == null || errcode == 0)) {
                        Swal.fire(

                            'Uspješno ',
                            'ste obrisali klijenta',
                            'success'
                        );
                    } else {
                        Swal.fire(message + '.' + errcode);
                    }
                    refresh();
                    prod();

                },
                error: function (xhr, textStatus, error) {
                    console.log(xhr.statusText);
                    console.log(textStatus);
                    console.log(error);
                },
                async: true
            });
        }
    })
}


//---------------- Edit predmeta ---------------

function editTema(ID_teme) {
    var tablicaT = '<div class="editTbl" ><table class="table table-hover"><tbody>';
    $.ajax({
        type: 'POST',
        url: url,
        data: {
            "projekt": "p_komercijalna",
            "procedura": "p_get_teme",
            "ID": ID_teme
        },

        success: function (data) {
            var jsonBody = JSON.parse(data);
            var errcode = jsonBody.h_errcode;
            var message = jsonBody.h_message;

            if (message == null || message == "", errcode == null || errcode == 0) {
                $.each(jsonBody.data, function (k, v) {
                    tablicaT = "<div class='content' id='container'>";
                    tablicaT += "<div class='form-popup'>";
                    tablicaT += "<form class='form-container' name='myForm' method='post'>";
                    tablicaT += '<input class="nestani" type="text" id="ID" value="' + v.ID + '" readonly></input>'
                    tablicaT += "<label><b>Naziv teme</b></label>";
                    tablicaT += '<input type="text" value="' + v.NAZIV + '" id="ucenik" required> ';
                    tablicaT += "<label><b>Profesor</b></label>";
                    tablicaT += '<input type="text" value="' + v.PROFESOR + '" id="razred" required> ';
                    tablicaT += "<label><b>Ime predmeta</b></label>";
                    tablicaT += '<input type="text" value="' + v.PREDMET + '" id="predmet" required> ';
                    tablicaT += "<button class='btn' id='spremiTemu'><i class='fa-solid fa-floppy-disk'></i> Unesi</button><button class='cancel' id='odustani' onClick='teme()'><i class='fa-solid fa-ban'></i></i> Odustani</button></form></div></div>";
         
                });
                $("#container").html(tablicaT);
            } else {
                if (errcode == 999) {
                    $("#container").html(loginForm);
                } else {
                    Swal.fire(message + '.' + errcode);
                }
            }
            //refresh();
        },
        error: function (xhr, textStatus, error) {
            console.log(xhr.statusText);
            console.log(textStatus);
            console.log(error);
        },
        async: true

    });
}

  