
var inData_prof = { projekt: "p_komercijalna", procedura: "p_get_ucenici" };

function ekran_prof() {
  $.ajax({
    type: "POST",
    url: url,
    data: inData_prof,
    success: function (data) {
      var jsonBody = JSON.parse(data);
      var errcode = jsonBody.h_errcode;
      var message = jsonBody.h_message;
      var count = jsonBody.count;

      var izlaz_prof = inicijalizacijaProfesor();

      if ((message == null || message == "", errcode == null || errcode == 0)) {
        document.getElementById("container").innerHTML = izlaz_prof;

        $("#example").DataTable({
          data: jsonBody.data,
          columns: [
            { data: "TEMA" },
            { data: "UCENIK" },
            { data: "PREDMET" },
            { data: "SMJER" },
          ],

          language: {
            decimal: "",
            emptyTable: "No data available in table",
            info: "",
            infoEmpty: "",
            infoFiltered: "",
            infoPostFix: "",
            thousands: ",",
            lengthMenu: "Prikaži _MENU_ zapisa",
            loadingRecords: "Učitavajne...",
            processing: "Processing...",
            search: "Pretraga:",
            zeroRecords: "Nema rezultata pretragi",
            paginate: {
              first: "Prvi",
              last: "Drugi",
              next: "Slijedeći",
              previous: "Prethodni",
            },
            aria: {
              sortAscending: ": activate to sort column ascending",
              sortDescending: ": activate to sort column descending",
            },
          },
        });
      } else {
        if (errcode == 999) {
          $("#container").html(loginForm);
        } else {
          Swal.fire(message + "." + errcode);
        }
      }
    },
    error: function (xhr, textStatus, error) {
      console.log(xhr.statusText);
      console.log(textStatus);
      console.log(error);
    },
    async: false,
  });
}

function inicijalizacijaProfesor() {
  return (
    "<div class='content'><div class='flex'><p class='naziv'>Učenici</p></div><div class='tbl'><div class='por'><table id='example' class='display table-hover cell-border ' style='width:100%'><thead><tr>" +
    "<th>TEMA</th>" +
    " <th>UCENIK</th>" +
    " <th>PREDMET</th>" +
    " <th>SMJER</th>" +
    "</thead></table></div> </div> <div class='flex'><button  onClick='logout()' class='logout mm' ><i class='fa-solid fa-right-from-bracket'></i>  Logout</button></div>"
  );
}


var inDataTema = { projekt: "p_komercijalna", procedura: "p_get_teme" };

function teme_prof() {
  $.ajax({
    type: "POST",
    url: url,
    data: inDataTema,
    success: function (data) {
      var jsonBody = JSON.parse(data);
      var errcode = jsonBody.h_errcode;
      var message = jsonBody.h_message;
      var count = jsonBody.count;

      var izlaz = inicijalizacijaTemaProf();

      if ((message == null || message == "", errcode == null || errcode == 0)) {
        document.getElementById("container").innerHTML = izlaz;
        $("#example").DataTable({
          data: jsonBody.data,
          columns: [{ data: "NAZIV" }, { data: "PREDMET" }],

          language: {
            decimal: "",
            emptyTable: "No data available in table",
            info: "",
            infoEmpty: "",
            infoFiltered: "",
            infoPostFix: "",
            thousands: ",",
            lengthMenu: "Prikaži _MENU_ zapisa",
            loadingRecords: "Učitavajne...",
            processing: "Processing...",
            search: "Pretraga:",
            zeroRecords: "Nema rezultata pretragi",
            paginate: {
              first: "Prvi",
              last: "Drugi",
              next: "Slijedeći",
              previous: "Prethodni",
            },
            aria: {
              sortAscending: ": activate to sort column ascending",
              sortDescending: ": activate to sort column descending",
            },
          },
        });
      } else {
        if (errcode == 999) {
          $("#container").html(loginForm);
        } else {
          Swal.fire(message + "." + errcode);
        }
      }
    },
    error: function (xhr, textStatus, error) {
      console.log(xhr.statusText);
      console.log(textStatus);
      console.log(error);
    },
    async: false,
  });
}

function inicijalizacijaTemaProf() {
  return (
    "<div class='content'><div class='flex'><p class='naziv'>Teme</p></div><div class='tbl'><div class='por'><table id='example' class='display table-hover cell-border ' style='width:100%'><thead><tr>" +
    " <th>NAZIV</th>" +
    "<th>PREDMET</th></tr></thead></table></div></div></div>"
  );
}
