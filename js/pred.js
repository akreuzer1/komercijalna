
var inData = { projekt: "p_komercijalna", procedura: "p_get_predmeti" };

//
var ID_predmet;
var ID_predmeta;
//---------------- Dohvaćanje podataka iz baze -----------

function predmeti() {
  $.ajax({
    type: "POST",
    url: url,
    data: inData,
    success: function (data) {
      var jsonBody = JSON.parse(data);
      var errcode = jsonBody.h_errcode;
      var message = jsonBody.h_message;
      var count = jsonBody.count;

      var izlaz = inicijalizacija();

      if ((message == null || message == "", errcode == null || errcode == 0)) {
        document.getElementById("container").innerHTML = izlaz;

        $("#example").DataTable({
          data: jsonBody.data,
          columns: [
            { data: "ID" },
            { data: "PREDMET" },
          
            { data: "SMJER" },

            {
              data: "ACTION",
              orderable: false,
              autoWidth: false,
            },
            {
              data: "ACTIONN",
              orderable: false,
              autoWidth: false,
            },
          ],
          columnDefs: [
            { width: "20px", targets: 3, className: "dt-center" },
            { width: "20px", targets: 4, className: "dt-center" },
          ],
          language: {
            decimal: "",
            emptyTable: "Nema podataka u tablici",
            info: "",
            infoEmpty: "",
            infoFiltered: "",
            infoPostFix: "",
            thousands: ",",
            lengthMenu: "Prikaži _MENU_ zapisa",
            loadingRecords: "Učitavajne...",
            processing: "Processing...",
            search: "Pretraga:",
            zeroRecords: "Nema rezultata pretragi",
            paginate: {
              first: "Prvi",
              last: "Drugi",
              next: "Slijedeći",
              previous: "Prethodni",
            },
            aria: {
              sortAscending: ": activate to sort column ascending",
              sortDescending: ": activate to sort column descending",
            },
          },
        });
      } else {
        if (errcode == 999) {
          $("#container").html(loginForm);
        } else {
          Swal.fire(message + "." + errcode);
        }

      }
     
    },
    error: function (xhr, textStatus, error) {
      console.log(xhr.statusText);
      console.log(textStatus);
      console.log(error);
    },
    async: false,
  });
}

function inicijalizacija() {
  return (
    "<div class='content'><div class='flex'><p class='naziv'>Predmeti</p></div><div class='tbl'><div class='por'><table id='example' class='display table-hover cell-border ' style='width:100%'><thead><tr>" +
    " <th>ID</th>" +
    " <th>PREDMET</th>" +
   
    " <th>SMJER</th><th></th><th></th>" +
    "</tr></thead></table></div><button class='unesi_prof' onClick='insertPred()'><i class='fa-solid fa-download'></i>  Unesi predmet</button> </div></div>"
  );
}

//--------forma za unos predmeta

function insertPred() {
  var unos = "<div class='content' id='container'>";
  unos += "<div class='form-popup'>";
  unos += "<form class='form-container' name='myForm' method='post'>";
  unos += "<label><b>Naziv predmeta</b></label>";
  unos += "<input type='text' placeholder='Naziv predmeta' id='naziv'> ";
  //unos += "<label><b>Smjer</b></label>";
  // unos += "<input type='number' placeholder='Smjer' id='smjer'>";
  unos += "<label for='smjer'><b>Smjer</b><br></label>";
  unos += "<select class='drop' id='smjer'>";
  unos += "<option value='1'>Komerijalisti</option>";
  unos += "<option value='2'>Prodavači</option>";
  unos += "</select>";
  unos +=
    "<button class='btn' id='spremiPred'><i class='fa-solid fa-floppy-disk'></i> Unesi</button><button class='cancel' id='odustani' onClick='predmeti()'><i class='fa-solid fa-ban'></i></i> Odustani</button></form></div></div> ";
  $("#container").html(unos);

  

  $(document).ready(function() {
    $('#drop').select2();
    $('select').select2({  

        language: {
      
          noResults: function() {
      
            return "Nema rezultata";        
          },
          searching: function() {
      
            return "Tražim..";
          }
        }
      });

});


}

$(document).on("click", "#spremiPred", function () {
  var NAZIV_pred = $("#naziv").val();
  var SMJER_pred = $("#smjer").val();
  var ID_pred = $("#ID").val();

  if (NAZIV_pred == null || NAZIV_pred == "") {
    Swal.fire("Molimo unesite naziv predmeta");
  } else if (SMJER_pred == null || SMJER_pred == "") {
    Swal.fire("Molimo unesite smjer");
  } else {
    $.ajax({
      type: "POST",
      url: url,
      data: {
        projekt: "p_komercijalna",
        procedura: "p_save_predmeti",
        ID: ID_pred,
        naziv: NAZIV_pred,
        smjer: SMJER_pred,
      },
      success: function (data) {
        var jsonBody = JSON.parse(data);
        var errcode = jsonBody.h_errcode;
        var message = jsonBody.h_message;
        console.log(data);

        if (
          (message == null || message == "") &&
          (errcode == null || errcode == 0)
        ) {
          Swal.fire("Uspješno se unijeli korisnika");
        } else {
          Swal.fire(message + "." + errcode);
        }
        refresh();
        predmeti();
      },
      error: function (xhr, textStatus, error) {
        console.log(xhr.statusText);
        console.log(textStatus);
        console.log(error);
      },
      async: true,
    });
  }
});

//---------------- Brisanje predmeta ---------------

function delPredmet(ID_predmet) {
  Swal.fire({
    // title: 'Želite li zaista obrisati predmet?',
    title:
      "<h5 style='color:black'>" +
      "Želite li zaista obrisati predmet?" +
      "</h5>",
    icon: "warning",
    showCancelButton: true,
    confirmButtonColor: '#2E2D7A',
    background: '#CBCBCB',
    cancelButtonColor: '#ED1C24',
    confirmButtonText: "Da, obriši predmet!",
    cancelButtonText: "Odustani!",
    animation: true,
  }).then((result) => {
    if (result.isConfirmed) {
      $.ajax({
        type: "POST",
        url: url,
        data: {
          projekt: "p_komercijalna",
          ID: ID_predmet,
          procedura: "p_obrisi_predmet",
        },

        success: function (data) {
          var jsonBody = JSON.parse(data);
          var errcode = jsonBody.h_errcode;
          var message = jsonBody.h_message;
          console.log(data);
          console.log(ID_predmet);

          if (
            (message == null || message == "") &&
            (errcode == null || errcode == 0)
          ) {
            Swal.fire("Uspješno ", "ste obrisali klijenta", "success");
          } else {
            Swal.fire(message + "." + errcode);
          }
          refresh();
          prod();
        },
        error: function (xhr, textStatus, error) {
          console.log(xhr.statusText);
          console.log(textStatus);
          console.log(error);
        },
        async: true,
      });
    }
  });
}

//---------------- Edit predmeta ---------------

function editPredmet(ID_pred) {
  var tablicaPred = "<div class='content' id='container'>";
  $.ajax({
    type: "POST",
    url: url,
    data: {
      projekt: "p_komercijalna",
      procedura: "p_get_predmeti",
      ID: ID_pred,
    },

    success: function (data) {
      var jsonBody = JSON.parse(data);
      var errcode = jsonBody.h_errcode;
      var message = jsonBody.h_message;

      if ((message == null || message == "", errcode == null || errcode == 0)) {
        $.each(jsonBody.data, function (k, v) {
          tablicaPred += "<div class='form-popup'>";
          tablicaPred +=
            "<form class='form-container' name='myForm' method='post'>";
          tablicaPred +=
            '<input class="nestani" type="text" id="ID" value="' +
            v.ID +
            '" readonly></input>';
          tablicaPred += "<label><b>Naziv predmeta</b></label>";
          tablicaPred +=
            '<input type="text"  value="' + v.PREDMET + '" id="naziv">';
          //unos += "<label><b>Smjer</b></label>";
          // unos += "<input type='number' placeholder='Smjer' id='smjer'>";
          tablicaPred += "<label for='smjer'><b>Smjer</b><br></label>";
          tablicaPred +=
            '<select class="drop" value="' + v.SMJER + '" id="smjer">';
          tablicaPred += "<option value='1'>Komerijalisti</option>";
          tablicaPred += "<option value='2'>Prodavači</option>";
          tablicaPred += "</select>";
          tablicaPred +=
            "<button class='btn' id='spremiPred'><i class='fa-solid fa-floppy-disk'></i> Izmjeni</button><button class='cancel' id='odustani' onClick='predmeti()'><i class='fa-solid fa-ban'></i></i> Odustani</button></form></div></div> ";
        });
        $("#container").html(tablicaPred);
      } else {
        if (errcode == 999) {
          $("#container").html(loginForm);
        } else {
          Swal.fire(message + "." + errcode);
        }
      }
    },
    error: function (xhr, textStatus, error) {
      console.log(xhr.statusText);
      console.log(textStatus);
      console.log(error);
    },
    async: true,
  });
}
